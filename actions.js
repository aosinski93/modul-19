// actions

export const ADD_COMMENT = {
    type: ADD_COMMENT,
    text: 'My comment',
    id: uuid.v4()
}

export const REMOVE_COMMENT = {
    type: REMOVE_COMMENT,
    id: id
}

export const EDIT_COMMENT = {
    type: EDIT_COMMENT,
    id: id,
    text: 'editted comment'
}

export const THUMB_UP = {
    type: THUMB_UP,
    thumbUpCounter : thumbUpCounter + 1
}

export const THUMB_DOWN = {
    type: THUMB_DOWN,
    thumbDownCounter: thumbDownCounter + 1 
}

export const ADD_USER = {
    type: ADD_USER,
    id: uuid.v4(),
    firstName: 'John',
    lastName: 'Smith'
}
