import {ADD_COMMENT, REMOVE_COMMENT, EDIT_COMMENT, THUMB_DOWN, THUMB_UP} from './actions';

function comments(state = [], action) {
    switch(action.type) {
        case ADD_COMMENT:
            return [{
                        id: action.id,
                        text: action.text,
                        votes: 0
                    }, 
                    ...state.comments];            
        case REMOVE_COMMENT:
            return { comments: state.comments.filter(comment => comment.id !== action.id)};
            
        case EDIT_COMMENT:
            return {
                comments: state.comments.find(comment => { 
                    if (comment.id === action.id) { 
                        comment.text = action.text;
                   }
                })
            };   
        case THUMB_DOWN:
            return {
                comments: state.comments.find(comment => { 
                    if (comment.id === action.id) {                   
                         comment.votes -= 1;
                    }
                })
            };
        case THUMB_UP:
            return {
                comments: state.comments.find(comment => { 
                    if (comment.id === action.id) {                    
                        comment.votes += 1;
                    }
                })
            };
        default:
            return state;
    }
}