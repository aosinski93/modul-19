import React from 'react';
import Comment from './CommentContainer';

const CommentsList = ({comments, addComment}) => 
    <div>
        <button onClick={() => addComment(prompt('Write new comment'))}>New comment</button>
        <ul>
            {comments.map(comment => 
                <Comment key={comment.id} {...comment}/>
            )}
        </ul>
    </div>

export default CommentsList;