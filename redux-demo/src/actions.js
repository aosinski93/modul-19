import uuid from 'uuid';


//add comment
const ADD_COMMENT = "ADD_COMMENT";

function addComment(text){
    return {
        type: ADD_COMMENT,
        text,
        id: uuid.v4()
    }
}

export {ADD_COMMENT};
export {addComment};

// remove comment
const REMOVE_COMMENT = "REMOVE_COMMENT" 

function removeComment(id) {
    return {
        type: REMOVE_COMMENT,
        id
    }
}

export {REMOVE_COMMENT};
export {removeComment};

// edit comment 
const EDIT_COMMENT = "EDIT_COMMENT";

function editComment(id){
    return {
        type: EDIT_COMMENT,
        id,
        text: 'editted comment'
    }
}

export {EDIT_COMMENT};
export {editComment};

//thumb up
const THUMB_UP = "THUMB_UP";

function thumbUp(id) {
    return {
        type: THUMB_UP,
        id: id,
    }
}

export {THUMB_UP};
export {thumbUp};

//thumb down
const THUMB_DOWN = "THUMB_DOWN";
function thumbDown(id) {
    return {
        type: THUMB_DOWN,
        id: id 
    }
}

export {THUMB_DOWN};
export {thumbDown};

//add user
const ADD_USER = "ADD_USER" 
function addUser(firstName, lastName) {
    return {    
        type: ADD_USER,
        id: uuid.v4(),
        firstName: firstName,
        lastName: lastName
    }
}
export {ADD_USER};
export {addUser};
