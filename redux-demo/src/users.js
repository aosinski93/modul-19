import {ADD_USER} from './actions';

function users(state = [], action) {
    switch(action.type) {
        case ADD_USER:
            return [{
                id: action.id,
                firstName: action.firstName,
                lastName: action.lastName
            },
            ...state.users];
        default: 
            return state;
    }
    
}

export default users;